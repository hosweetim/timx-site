require('dotenv').config();

import { Express } from 'express';
import * as request from 'supertest';

import { Server } from '../server/server';
import * as http from 'http';
const config = require('../nuxt.config.js');

const TEST_PORT = 3000;
let app: Express;
let server: http.Server;

beforeAll(async () => {
    config.dev = true;
    config.build = {
        quiet: true,
    };

    app = await Server.start(config);
    server = app.listen(TEST_PORT);
}, 60000);

afterAll(() => {
    server.close();
});

describe('/blog', () => {
    it('should return 200', async () => {
        await request(app)
            .get('/blog')
            .expect('Content-Type', 'text/html; charset=utf-8')
            .expect(200);
    });
});

describe('/food', () => {
    it('should return 200', async () => {
        await request(app)
            .get('/food')
            .expect('Content-Type', 'text/html; charset=utf-8')
            .expect(200);
    });
});

describe('/links', () => {
    it('should return 200', async () => {
        await request(app)
            .get('/links')
            .expect('Content-Type', 'text/html; charset=utf-8')
            .expect(200);
    });
});

describe('/location', () => {
    it('should return 200', async () => {
        await request(app)
            .get('/location')
            .expect('Content-Type', 'text/html; charset=utf-8')
            .expect(200);
    });
});

describe('/api', () => {
    describe('/markdown', () => {
        describe('/theme', () => {
            it('should return 200', async () => {
                await request(app)
                    .get('/api/markdown/theme')
                    .expect('Content-Type', /json/)
                    .expect(200);
            });
        });
    });

    describe('/blog', () => {
        it('should return error without any filename', async () => {
            await request(app)
                .get('/api/blog')
                .expect(200);
        });
    });

    describe('/crawler', () => {
        it('should return list of supported crawler', async () => {
            await request(app)
                .get('/api/crawler')
                .expect('Content-Type', /json/)
                .expect(200);
        });

        it('should return crawl result from tabelog', async () => {
            await request(app)
                .post('/api/crawler')
                .send({
                    type: 'tabelog',
                    url: 'https://tabelog.com/en/tokyo/A1305/A130501/13109957/',
                })
                .expect('Content-Type', /json/)
                .expect(200);
        });

        describe('/tabelog', () => {
            it('should return crawl result from tabelog', async () => {
                await request(app)
                    .get('/api/crawler/tabelog/https%3A%2F%2Ftabelog.com%2Ftokyo%2FA1305%2FA130501%2F13125875%2F')
                    .expect('Content-Type', /json/)
                    .expect(200);
            });
        });
    });
});
