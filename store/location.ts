import {
    ActionTree,
    MutationTree,
} from 'vuex';

import {
    RootState,
    LocationDataState,
} from './types';

export interface LocationData {
    lat: number;
    lng: number;
}

export const state = (): LocationDataState => ({
    data: [],
    downloadPercentage: 0,
});

export const mutations: MutationTree<LocationDataState> = {
    // tslint:disable-next-line:no-shadowed-variable
    setLocationData(state, data: LocationData[]) {
        state.data = data;
    },
    // tslint:disable-next-line:no-shadowed-variable
    setDownloadProgress(state, percentage: number) {
        state.downloadPercentage = Math.ceil(percentage);
    },
};


export const actions: ActionTree<LocationDataState, RootState> = {
    // tslint:disable-next-line:no-shadowed-variable
    async getLocationData({ commit, state }, handler) {
        if (!state.data.length) {
            const locationData = await (this as any).$axios
                .$get(
                    '/api/storage/location_filter.json',
                    {
                        onDownloadProgress: ({ loaded, total }: { loaded: number, total: number }) => {
                            const percentage = Math.ceil((loaded / total) * 100);

                            if ((percentage % 10) === 0) {
                                commit('setDownloadProgress', percentage);
                            }
                        },
                    },
                );

            commit('setLocationData', locationData);
        }
    },
};
