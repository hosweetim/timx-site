import { RestaurantInfo } from '../server/models/restaurant/restaurant.schema';
import { LocationData } from './location';

// tslint:disable-next-line:no-empty-interface
export interface RootState {
}

export interface ProgressState {
    indeterminate: boolean;
    isLoading: boolean;
    loadingProgress: number | null;
    counter: number;
}

export interface RestaurantState {
    restaurants: RestaurantInfo[];
}

export interface LocationDataState {
    data: LocationData[];
    downloadPercentage: number;
}
