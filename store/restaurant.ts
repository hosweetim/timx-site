import {
    ActionTree,
    MutationTree,
    GetterTree,
} from 'vuex';

import {
    RootState,
    RestaurantState,
} from './types';

import { RestaurantInfo } from '~/server/models/restaurant/restaurant.schema';

export const state = (): RestaurantState => ({
    restaurants: [],
});

export const getters: GetterTree<RestaurantState, RootState> = {
    // tslint:disable-next-line:no-shadowed-variable
    getRestaurantByCategories(state) {
        const sortedCategories = state.restaurants
            .reduce((a: any, b) => {
                const { country } = b;
                a[country] = a[country] || [];
                a[country].push(b);

                return a;
            }, {});

        return Object.keys(sortedCategories)
            .map((x) => ({
                key: x,
                restaurants: sortedCategories[x],
            }))
            .sort((a, b) => {
                return b.restaurants.length - a.restaurants.length;
            });
    },
};

export const mutations: MutationTree<RestaurantState> = {
    // tslint:disable-next-line:no-shadowed-variable
    setRestaurant(state, data: RestaurantInfo[]) {
        state.restaurants = data;
    },
};


export const actions: ActionTree<RestaurantState, RootState> = {
    async nuxtServerInit({ commit }, { app }) {
        const data = await app.$axios.$get('/api/restaurant');

        commit('setRestaurant', data);
    },
};
