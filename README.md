# timX website

This is my personal website

# Prerequisite

Generate the `.env` file using the command below

```
bash scripts/download_env_file.sh
```


# To run

```
npm run dev
```
