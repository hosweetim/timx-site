#!/bin/bash
MAX_VERSION_COUNT_KEEP=${1:-3}

CHECK_CHAR="\\xE2\\x9C\\x94"
CROSS_CHAR="\\xE2\\x9C\\x98"
YELLOW_COLOR="\\e[1;33m"
GREEN_COLOR="\\e[1;32m"
RED_COLOR="\\e[1;31m"
DEFAULT_COLOR="\\e[0m"

VERSIONS=$(gcloud app versions list --sort-by '~version' --format 'value(version.id)')

printf "%sKeeping %s latest versions in GAE%s\\n" \
    "${YELLOW_COLOR}" \
    "${MAX_VERSION_COUNT_KEEP}" \
    "${DEFAULT_COLOR}"

COUNT=0
for VERSION in $VERSIONS
do
    ((COUNT++))
    if [ ${COUNT} -gt "${MAX_VERSION_COUNT_KEEP}" ]
    then
        printf "%s\\t%s%s%s\\n" \
            "${VERSION}" \
            "${RED_COLOR}" \
            "${CROSS_CHAR}" \
            "${DEFAULT_COLOR}"

        gcloud app versions delete "${VERSION}" -q
    else
        printf "%s\\t%s%s%s\\n" \
            "${VERSION}" \
            "${GREEN_COLOR}" \
            "${CHECK_CHAR}" \
            "${DEFAULT_COLOR}"
    fi
done
