#!/bin/bash

ENV_CONFIGURATION=${1:-"development"}
URL="gs://timxlab.appspot.com/env/${ENV_CONFIGURATION}/.env"

echo "Downloading ${ENV_CONFIGURATION} .env file..."
gsutil cp "${URL}" .env
