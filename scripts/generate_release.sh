#!/bin/bash

REPO_NAME=$1
CURRENT_TAG=$2
PRIVATE_TOKEN=$3
PROJECT_ID=$4

PREVIOUS_TAG=$(git describe --abbrev=0 --tags "${CURRENT_TAG}"^)

OUTPUT_FILENAME="RELEASE_${REPO_NAME}_${CURRENT_TAG}.md"

echo "Generating report for ${REPO_NAME}"
echo "Changes from ${PREVIOUS_TAG} to ${CURRENT_TAG}"
echo "Output release note to ${OUTPUT_FILENAME}"

COMMITED_ISSUES_LIST=$(git log --oneline "${PREVIOUS_TAG}".."${CURRENT_TAG}" | \
    grep -o "#[0-9]*" | \
    grep -o "[0-9]*" | \
    sort -u | \
    xargs \
        -I {} \
        -P "$(nproc)" \
        curl \
            --silent \
            -H "Private-Token: ${PRIVATE_TOKEN}" \
            --url "https://gitlab.com/api/v4/projects/${PROJECT_ID}/issues/{}" | \
                jq '[{ "name": .title, "type": (.labels | first), "link": .web_url }]' )

GROUP_ISSUE_TYPE_OUTPUT=$(echo "${COMMITED_ISSUES_LIST}" | \
    jq -s 'add | group_by(.type)[] | { (.[0].type): [ .[] | { "name": .name, "link": .link } ] }' | \
    jq -s add)

BUG_REPORT=$(echo "${GROUP_ISSUE_TYPE_OUTPUT}" | jq -r '.["bug"] | .[] | ("-  [" + .name + "](" + .link + ")") ')
FEATURE_REPORT=$(echo "${GROUP_ISSUE_TYPE_OUTPUT}" | jq -r '.["feature"] | .[] | ("-  [" + .name + "](" + .link + ")") ')
DEVOPS_REPORT=$(echo "${GROUP_ISSUE_TYPE_OUTPUT}" | jq -r '.["devops"] | .[] | ("-  [" + .name + "](" + .link + ")") ')

cat >> "${OUTPUT_FILENAME}" <<- EOM
# Release Note (${REPO_NAME} - ${PROJECT_ID}) 
Generated at $(date '+%d/%m/%Y %H:%M:%S')

Changes from ${PREVIOUS_TAG} to ${CURRENT_TAG}

## Implemented features
${FEATURE_REPORT}

## Bugs fixed
${BUG_REPORT}

## Operation improvement
${DEVOPS_REPORT}
EOM
