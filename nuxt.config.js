require('dotenv').config();

module.exports = {
    buildDir: 'dist/.nuxt',
    head: {
        title: 'timx',
        meta: [
            {
                charset: 'utf-8'
            },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1'
            },
            {
                hid: 'description',
                name: 'description',
                content: 'timx website'
            }
        ],
        script: [
            {
                src: 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAlVCBMD_TE7H7jcUzV1u37ZKBx5yHDNzQ&libraries=visualization'
            }
        ],
        link: [
            {
                rel: 'icon',
                type: 'image/x-icon',
                href: '/favicon.ico'
            },
            {
                rel: 'stylesheet',
                href: 'https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.2.0/css/flag-icon.min.css'
            }
        ]
    },
    loading: {
        color: '#3F51B5'
    },
    css: [
        'assets/main.css',
        {
            src: '~/node_modules/highlight.js/styles/vs2015.css',
            lang: 'css'
        }
    ],
    modules: [
        'nuxt-typescript',
        '@nuxtjs/axios',
        '@nuxtjs/dotenv',
        '@nuxtjs/google-analytics',
        '@nuxtjs/pwa',
    ],
    typescript: {
        "checker": false,
    },
    axios: {
        proxy: true
    },
    'google-analytics': {
        id: 'UA-49860919-7'
    },
    plugins: [
        '~/plugins/vuetify'
    ],
}
