import {
    Request,
    Response,
    NextFunction,
} from 'express';

import { RestaurantModel } from '../../models';

export const RestaurantController = {
    async get(req: Request, res: Response, next: NextFunction) {
        const restaurants = await RestaurantModel.get();

        res.json(restaurants);
    },
    async post(req: Request, res: Response, next: NextFunction) {
        const [ err, result ] = await RestaurantModel.create(req.body);

        if (err) {
            next(err);
            return;
        }

        res.json(result);
    },
    async put(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;
        const { restaurant } = req.body;

        const err = await RestaurantModel.put(id, restaurant);

        if (err) {
            next(err);
            return;
        }

        res.json(restaurant);
    },
    async delete(req: Request, res: Response, next: NextFunction) {
        const { id } = req.params;

        const err = await RestaurantModel.delete(id);

        if (err) {
            next(err);
            return;
        }

        res.json({
            id,
        });
    },
};
