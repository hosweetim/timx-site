import { mocked } from 'ts-jest/utils';

import { LinksController } from './links.controller';
import { LinksModel } from '../../models';
import {
    response,
    next,
    resetMock,
} from '../../mocks';

jest.mock(
    '../../models',
    () => ({
        LinksModel: {
            create: jest.fn(),
            deleteById: jest.fn(),
        },
    }),
);

const mockedLinksModel = mocked(LinksModel, true);

afterEach(() => {
    mockedLinksModel.create.mockReset();
    mockedLinksModel.deleteById.mockReset();

    resetMock.all();
});

describe('deleteById', () => {
    it('should response if no error', async () => {
        const error = undefined;
        const req: any = {
            params: {
                id: '',
            },
        };

        mockedLinksModel.deleteById
            .mockReturnValue(error);

        expect.assertions(2);
        await LinksController.deleteById(req, response, next);

        expect(next).not.toBeCalled();
        expect(response.json).toBeCalled();
    });

    it('should called next when error is returned', async () => {
        const error = new Error();
        const req: any = {
            params: {
                id: '',
            },
        };

        mockedLinksModel.deleteById
            .mockReturnValue(error);

        expect.assertions(2);
        await LinksController.deleteById(req, response, next);

        expect(next).toBeCalledWith(error);
        expect(response.json).not.toBeCalled();
    });
});

describe('post', () => {
    it('should throw error when body is empty', async () => {
        const req: any = {
            body: {
            },
        };

        expect.assertions(3);
        await expect(LinksController.post(req, response, next))
            .rejects
            .toThrowError();

        expect(next).not.toBeCalled();
        expect(response.json).not.toBeCalled();
    });

    it('should throw error when body filed is empty string', async () => {
        const req: any = {
            body: {
                category: '',
                title: '',
                url: '',
            },
        };

        expect.assertions(3);
        await expect(LinksController.post(req, response, next))
            .rejects
            .toThrowError();

        expect(next).not.toBeCalled();
        expect(response.json).not.toBeCalled();
    });

    it('should return a created link', async () => {
        const req: any = {
            body: {
                catergory: 'category',
                title: 'title',
                url: 'url',
            },
        };

        const error = null;
        const result = {};
        mockedLinksModel.create
            .mockReturnValue([ error, result ]);

        expect.assertions(2);
        await LinksController.post(req, response, next);

        expect(next).not.toBeCalled();
        expect(response.json).toBeCalledWith(result);
    });

    it('should called next when error is returned', async () => {
        const req: any = {
            body: {
                catergory: 'category',
                title: 'title',
                url: 'url',
            },
        };

        const error = new Error();
        const result = {};
        mockedLinksModel.create
            .mockReturnValue([ error, result ]);

        expect.assertions(2);
        await LinksController.post(req, response, next);

        expect(next).toBeCalled();
        expect(response.json).not.toBeCalled();
    });
});
