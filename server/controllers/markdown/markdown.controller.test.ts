import { mocked } from 'ts-jest/utils';

import { MarkdownController } from './markdown.controller';
import { MarkdownModel } from '../../models';

import {
    response,
    next,
    resetMock,
} from '../../mocks';

jest.mock(
    '../../models',
    () => ({
        MarkdownModel: {
            render: jest.fn(),
            getHighlightTheme: jest.fn(),
        },
    }));

const mockedMarkdownModel = mocked(MarkdownModel, true);

afterEach(() => {
    mockedMarkdownModel.render.mockReset();
    mockedMarkdownModel.getHighlightTheme.mockReset();

    resetMock.all();
});

function createMockRequest(markdown: any, theme: any, withCss: any): any {
    return {
        body: {
            markdown,
        },
        query: {
            theme,
            withCss,
        },
    };
}

describe('post', () => {
    it('should reject if markdown is undefined', async () => {
        const req = createMockRequest(undefined, undefined, undefined);

        expect.assertions(1);
        await MarkdownController.post(req, response, next);

        expect(next).toBeCalled();
    });

    it('should reject if markdown is number', async () => {
        const req = createMockRequest(123, undefined, undefined);

        expect.assertions(1);
        await MarkdownController.post(req, response, next);

        expect(next).toBeCalled();
    });

    it('should response send and not called next with proper markdown data', async () => {
        const req = createMockRequest('# sample', undefined, undefined);

        expect.assertions(2);
        await MarkdownController.post(req, response, next);

        expect(next).not.toBeCalled();
        expect(response.send).toBeCalled();
    });
});

