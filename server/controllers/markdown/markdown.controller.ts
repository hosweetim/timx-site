import {
    Request,
    Response,
    NextFunction,
} from 'express';

import { MarkdownModel } from '../../models';

export const MarkdownController = {
    async post(req: Request, res: Response, next: NextFunction) {
        const { markdown } = req.body;
        const { theme, withCss } = req.query;

        if (typeof markdown === 'string') {
            const result = MarkdownModel.render(markdown, {
                withCss,
                theme,
            });

            res.set('Content-Type', 'text/html');
            res.send(result);
            return;
        }

        next(new Error('Please provide markdown field as string type'));
    },
    theme: {
        async get(req: Request, res: Response, next: NextFunction) {
            const themes = MarkdownModel.getHighlightTheme();

            res.json(themes);
        },
    },
};
