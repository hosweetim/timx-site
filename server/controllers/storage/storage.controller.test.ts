import { mocked } from 'ts-jest/utils';

import { StorageModel } from '../../models';
import { StorageController } from './storage.controller';
import {
    response,
    next,
    resetMock,
} from '../../mocks';

jest.mock(
    '../../models',
    () => ({
        StorageModel: {
            isExist: jest.fn(),
            getMetadata: jest.fn(),
            createFileReadStream: jest.fn().mockReturnValue({
                pipe: jest.fn(),
            }),
        },
    }));

const mockedStorageModel = mocked(StorageModel, true);

afterEach(() => {
    mockedStorageModel.isExist.mockReset();
    mockedStorageModel.getMetadata.mockReset();

    resetMock.all();
});

describe('getByFileName', () => {
    it('should call next when fileName is not found', async () => {
        const req: any = {
            params: {
            },
        };

        mockedStorageModel.isExist.mockReturnValue(false);

        expect.assertions(1);
        await StorageController.getByFileName(req, response, next);

        expect(next).toBeCalled();
    });

    it('should res when fileName is found', async () => {
        const size = 100;
        const fileName = 'sample';
        const req: any = {
            params: {
                fileName,
            },
        };

        mockedStorageModel.isExist.mockReturnValue(true);
        mockedStorageModel.getMetadata.mockReturnValue({
            size,
        });

        expect.assertions(3);
        await StorageController.getByFileName(req, response, next);

        expect(mockedStorageModel.getMetadata).toBeCalledWith(fileName);
        expect(response.set).toBeCalledWith('Content-Length', size);
        expect(next).not.toBeCalled();
    });
});

