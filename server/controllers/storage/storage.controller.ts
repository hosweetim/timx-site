import {
    Request,
    Response,
    NextFunction,
} from 'express';

import { StorageModel } from '../../models';

export const StorageController = {
    async get(req: Request, res: Response, next: NextFunction) {
        const result = await StorageModel.getFiles();

        const files = await Promise.all(
            result.map((f: any) => StorageModel.getMetadata(f.name)));

        res.json(files);
    },
    async getByFileName(req: Request, res: Response, next: NextFunction) {
        const { fileName } = req.params;

        const isExist = await StorageModel.isExist(fileName);

        if (isExist) {
            const { size } = await StorageModel.getMetadata(fileName);
            const storageStream = StorageModel.createFileReadStream(fileName);

            res.set('Content-Length', size);
            storageStream.pipe(res);
            return;
        }

        next(new Error(`${fileName} not found`));
    },
};
