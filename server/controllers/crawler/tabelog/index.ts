import axios from 'axios';

import { CrawlerHandler } from '../type';
import { TabelogModel } from '../../../models';


export const TabelogCrawler: CrawlerHandler = async (url: string) => {
    const englishUrl = TabelogModel.convertToEnglishUrl(url);
    const { data } = await axios.get(englishUrl);

    return TabelogModel.crawl(data);
};
