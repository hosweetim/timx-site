export interface CrawlerHandlerMap {
    [key: string]: CrawlerHandler;
}

export type CrawlerHandler = (url: string) => Promise<any>;
