require('dotenv').config();

const consola = require('consola');
import { Server } from './server';

const config = require('../nuxt.config.js');
config.dev = !(process.env.NODE_ENV === 'production');

const host = process.env.HOST || '127.0.0.1';
const port = Number(process.env.PORT) || 3000;

async function start() {
    const app = await Server.start(config);
    app.listen(port, host, () => {
        consola.ready({
            message: `Server listening on http://${host}:${port}`,
            badge: true,
        });
    });
}

start()
    .catch((e) => {
        consola.error({
            message: e,
            badge: true,
        });
    });
