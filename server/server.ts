
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as compression from 'compression';

const { Nuxt, Builder } = require('nuxt');

import { MongoDbConnection } from './config/connection/mongodb';
import routes from './routes';
import { errorMiddleware } from './middleware';

export const Server = {
    async start(config: any) {
        const app = express();
        const nuxt = new Nuxt(config);

        if (config.dev) {
            const builder = new Builder(nuxt);
            await builder.build();
        }

        const mongoDbError = await MongoDbConnection.connect();
        if (mongoDbError) {
            throw new Error(mongoDbError);
        }

        app.use(compression());
        app.use(bodyParser.urlencoded({
            extended: true,
        }));
        app.use(bodyParser.json());
        app.use(routes);
        app.use(nuxt.render);
        app.use(errorMiddleware);

        return app;
    },
};
