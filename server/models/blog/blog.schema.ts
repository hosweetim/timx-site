import * as Joi from 'joi';

export interface BlogInfo {
    title: string;
    description: string;
    tag: string;
}

export const BlogInfoSchema = Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().required(),
    tag: Joi.string().required(),
});
