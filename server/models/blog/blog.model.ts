
import * as Joi from 'joi';
import { Datastore } from '@google-cloud/datastore';

import {
    BlogInfoSchema,
    BlogInfo,
} from './blog.schema';

import { StorageModel } from '../storage/storage.model';

const {
    CLOUD_STORAGE_PROJECT_ID,
} = process.env;

if (!CLOUD_STORAGE_PROJECT_ID) {
    throw new Error('Google cloud datastore project ID environment key not set');
}

const dataStore = new Datastore({
    projectId: CLOUD_STORAGE_PROJECT_ID,
});

const BLOG_STORAGE_KIND = 'blog';
const BLOG_STORAGE_FOLDER = 'blog';

export const BlogModel = {
    getFilePath(fileName: string | undefined): string {
        return `${BLOG_STORAGE_FOLDER}/${fileName || ''}`;
    },
    async get() {
        const query = dataStore.createQuery(BLOG_STORAGE_KIND)
            .order('created');

        const [ blogs ] = await dataStore.runQuery(query);

        return blogs.map((x: any) => {
            const taskKey = x[dataStore.KEY];

            return {
                id: taskKey.id,
                ...x,
            };
        });
    },
    async getById(id: string) {
        const query = dataStore.createQuery(BLOG_STORAGE_KIND)
            .filter('__key__', '=', dataStore.key([ BLOG_STORAGE_KIND, Number(id) ]))
            .limit(1);

        const [ result ] = await dataStore.runQuery(query);
        const path = this.getFilePath(id);

        return {
            path,
            result,
        };
    },
    async create(blog: BlogInfo, file: Buffer) {
        const { error, value } = Joi.validate<BlogInfo>(blog, BlogInfoSchema);

        if (error) {
            throw(error);
        }

        const { tag, ...others } = value;

        const key = dataStore.key([ BLOG_STORAGE_KIND ]);

        const entity = {
            key,
            data: {
                created: new Date().toJSON(),
                ...others,
                tag: tag.split(',').map((token) => token.trim()),
            },
        };

        await dataStore.save(entity);

        const path = this.getFilePath(key.id);
        await StorageModel.uploadFile(path, file);

        return {
            id: key.id,
            ...entity.data,
        };
    },
    async delete(id: number) {
        const key = dataStore.key([ BLOG_STORAGE_KIND, id ]);

        await dataStore.delete(key);

        const path = this.getFilePath(id.toString());

        const [ isExist ] = await StorageModel.isExist(path);
        if (isExist) {
            const result = await StorageModel.deleteFile(path);
        }

        return id;
    },
    async getFile(id: string): Promise<string> {
        const path = this.getFilePath(id);
        const [ file ] = await StorageModel.getFile(path);

        return file.toString();
    },
};
