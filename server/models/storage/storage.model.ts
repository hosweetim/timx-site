import { Readable } from 'stream';

import { CloudStorageConnection } from '../../config/connection';
import { Config } from '../../config/env';

const { CLOUD_STORAGE_FOLDER_NAME } = Config;

export const StorageModel = {
    getFilePath(fileName: string): string {
        const regex = new RegExp(`^${CLOUD_STORAGE_FOLDER_NAME}\/`);
        const name = fileName.replace(regex, '');

        return `${CLOUD_STORAGE_FOLDER_NAME}/${name}`;
    },
    async uploadFile(path: string, file: Buffer) {
        const writeStream = CloudStorageConnection
            .getInstance()
            .file(this.getFilePath(path))
            .createWriteStream();

        const readableStream = new Readable();
        readableStream.push(file);
        readableStream.push(null);

        await new Promise((resolve, reject) => {
            writeStream.on('error', reject);
            writeStream.on('finish', resolve);

            readableStream.pipe(writeStream);
        });
    },
    async getFiles() {
        const [ result ] = await CloudStorageConnection
            .getInstance()
            .getFiles({
                prefix: `${CLOUD_STORAGE_FOLDER_NAME}/`,
            });

        return result;
    },
    async getFile(fileName: string): Promise<[Buffer]> {
        const filePath = this.getFilePath(fileName);

        return CloudStorageConnection
            .getInstance()
            .file(filePath)
            .download();
    },
    async deleteFile(fileName: string) {
        const filePath = this.getFilePath(fileName);

        return CloudStorageConnection
            .getInstance()
            .file(filePath)
            .delete();
    },
    async isExist(fileName: string): Promise<[boolean]> {
        const filePath = this.getFilePath(fileName);

        return CloudStorageConnection
            .getInstance()
            .file(filePath)
            .exists();
    },
    async getMetadata(fileName: string) {
        const filePath = this.getFilePath(fileName);

        const metaData = await CloudStorageConnection
            .getInstance()
            .file(filePath)
            .getMetadata();

        const {
            name,
            size,
            timeCreated,
            updated,
            md5hash,
        } = metaData[0];

        return {
            folder: CLOUD_STORAGE_FOLDER_NAME,
            name: name.replace(new RegExp(`${CLOUD_STORAGE_FOLDER_NAME}\/`), ''),
            size,
            timeCreated,
            updated,
            md5hash,
        };
    },
    createFileReadStream(fileName: string) {
        const filePath = this.getFilePath(fileName);

        return CloudStorageConnection
            .getInstance()
            .file(filePath)
            .createReadStream();
    },
};
