require('dotenv').config();

import { StorageModel } from './storage.model';

describe('getFilePath', () => {
    it('remove same folder name', () => {
        const CASES = [
            [ 'timx/blog/ahaha', 'timx/blog/ahaha' ],
        ];

        CASES.forEach((x) => {
            expect(StorageModel.getFilePath(x[0])).toBe(x[1]);
        });
    });
});
