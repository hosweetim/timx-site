import { ObjectId } from 'mongodb';

import { MongoDbConnection } from '../../config/connection';
import { Config } from '../../config/env';

export interface Links {
    category: string;
    title: string;
    url: string;
}

const { MONGODB_COLLECTION_LINKS } = Config;

export class LinksModel {
    static async get(): Promise<any[]> {
        return MongoDbConnection.getInstanceByCollection(MONGODB_COLLECTION_LINKS)
            .find()
            .toArray();
    }

    static async getById(id: string): Promise<any[]> {
        return MongoDbConnection.getInstanceByCollection(MONGODB_COLLECTION_LINKS)
            .find({
                _id: new ObjectId(id),
            })
            .toArray();
    }

    static async create(model: Links): Promise<[null | Error, null | any]> {
        const { category, title, url } = model;

        if (!category
            && !title
            && !url)
        {
            throw new Error('Not correct field, please provide category, title and link');
        }

        const result = await MongoDbConnection.getInstanceByCollection(MONGODB_COLLECTION_LINKS)
            .insertOne({
                category,
                title,
                url,
                createdAt: new Date(Date.now()),
            });

        if (result.result.ok > 0
            && result.result.n === 1)
        {
            return [ null, result.ops ];
        }

        return [ new Error('insertion failed'), null ];
    }

    static async deleteById(id: string): Promise<Error | null> {
        const result = await MongoDbConnection.getInstanceByCollection(MONGODB_COLLECTION_LINKS)
            .deleteOne({
                _id: new ObjectId(id),
            });

        if (result.result.ok
            && result.result.n === 1)
        {
            return null;
        }

        return new Error(`${id} delete failed`);
    }
}
