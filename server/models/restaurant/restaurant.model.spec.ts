import { mocked } from 'ts-jest/utils';
import { ObjectId } from 'mongodb';

import { MongoDbConnection } from '../../config/connection';

import { RestaurantModel } from './restaurant.model';
import { RestaurantInfo } from './restaurant.schema';

jest.mock('../../config/connection', () => ({
    MongoDbConnection: {
        getInstanceByCollection: jest.fn(),
    },
}));

const mockedMongoDbConnection = mocked(MongoDbConnection, true);

afterEach(() => {
    mockedMongoDbConnection.getInstanceByCollection.mockReset();
});

describe('get', () => {
    it('should return the data to be of RestaurantInfo type', async () => {
        const expected = [{
            name: 'Obondegohan',
            url: 'https://tabelog.com/en/tokyo/A1305/A130501/13138302/',
            country: 'jp',
            address: '1-11-1 Nishiikebukuro Toshima Tokyo',
            link: 'http://obon-de-gohan.com/index.php',
            rating: 8,
            holiday: '不定休（ルミネ池袋店に準ずる）',
            isRelocated: false,
            location: {
                lat: 35.728854119159735,
                lng: 139.70947684349488,
            },
        }];

        const input = [{
            name: 'Obondegohan',
            url: 'https://tabelog.com/en/tokyo/A1305/A130501/13138302/',
            country: 'jp',
            address: '1-11-1 Nishiikebukuro Toshima Tokyo',
            link: 'http://obon-de-gohan.com/index.php',
            rating: 8,
            holiday: '不定休（ルミネ池袋店に準ずる）',
            isRelocated: false,
            location: {
                type: 'Point',
                coordinates: [
                    139.70947684349488,
                    35.728854119159735,
                ],
            },
        }];

        mockedMongoDbConnection.getInstanceByCollection.mockReturnValue({
            find: () => ({
                toArray: jest.fn().mockReturnValue(input),
            }),
        });

        expect.assertions(1);
        const restaurants = await RestaurantModel.get();
        expect(restaurants).toStrictEqual(expected);
    });
});

describe('create', () => {
    it('should throw error when validation is failed', async () => {
        const input: any = {};

        expect.assertions(1);
        await expect(RestaurantModel.create(input))
            .rejects
            .toThrowError();
    });

    it('should not throw error when validation is correct', async () => {
        const expected = {};
        const input: RestaurantInfo = {
            name: 'Obondegohan',
            url: 'https://tabelog.com/en/tokyo/A1305/A130501/13138302/',
            country: 'jp',
            address: '1-11-1 Nishiikebukuro Toshima Tokyo',
            link: 'http://obon-de-gohan.com/index.php',
            rating: 8,
            holiday: '不定休（ルミネ池袋店に準ずる）',
            isRelocated: false,
            location: {
                lat: 35.728854119159735,
                lng: 139.70947684349488,
            },
        };

        const expectedInput = {
            name: 'Obondegohan',
            url: 'https://tabelog.com/en/tokyo/A1305/A130501/13138302/',
            country: 'jp',
            address: '1-11-1 Nishiikebukuro Toshima Tokyo',
            link: 'http://obon-de-gohan.com/index.php',
            rating: 8,
            holiday: '不定休（ルミネ池袋店に準ずる）',
            isRelocated: false,
            location: {
                type: 'Point',
                coordinates: [
                    139.70947684349488,
                    35.728854119159735,
                ],
            },
        };

        mockedMongoDbConnection.getInstanceByCollection.mockReturnValue({
            insertOne: jest.fn(() => ({
                result: {
                    ok: 1,
                    n: 1,
                },
                ops: expected,
            })),
        });

        expect.assertions(3);
        const [ error, actual ] = await RestaurantModel.create(input);

        expect(actual).toStrictEqual(expected);
        expect(error).toBeNull();
        expect((mockedMongoDbConnection as any).getInstanceByCollection().insertOne).toBeCalledWith(expectedInput);
    });

    it('should not throw error when validation with link as empty string', async () => {
        const expected = {};
        const input: RestaurantInfo = {
            name: 'Obondegohan',
            url: 'https://tabelog.com/en/tokyo/A1305/A130501/13138302/',
            country: 'jp',
            address: '1-11-1 Nishiikebukuro Toshima Tokyo',
            link: '',
            rating: 8,
            holiday: '不定休（ルミネ池袋店に準ずる）',
            isRelocated: false,
            location: {
                lat: 35.728854119159735,
                lng: 139.70947684349488,
            },
        };

        const expectedInput = {
            name: 'Obondegohan',
            url: 'https://tabelog.com/en/tokyo/A1305/A130501/13138302/',
            country: 'jp',
            address: '1-11-1 Nishiikebukuro Toshima Tokyo',
            link: '',
            rating: 8,
            holiday: '不定休（ルミネ池袋店に準ずる）',
            isRelocated: false,
            location: {
                type: 'Point',
                coordinates: [
                    139.70947684349488,
                    35.728854119159735,
                ],
            },
        };

        mockedMongoDbConnection.getInstanceByCollection.mockReturnValue({
            insertOne: jest.fn(() => ({
                result: {
                    ok: 1,
                    n: 1,
                },
                ops: expected,
            })),
        });

        expect.assertions(3);
        const [ error, actual ] = await RestaurantModel.create(input);

        expect(actual).toStrictEqual(expected);
        expect(error).toBeNull();
        expect((mockedMongoDbConnection as any).getInstanceByCollection().insertOne).toBeCalledWith(expectedInput);
    });
});

describe('put', () => {
    it('should throw error when validation is failed', async () => {
        const input: any = {};
        const id = '';

        expect.assertions(1);
        await expect(RestaurantModel.put(id, input))
            .rejects
            .toThrowError();
    });

    it('should not throw error when validation is correct', async () => {
        const expected = {};
        const id = '5bc4a7ef6b3a7c2f9c04ef13';

        const input: RestaurantInfo = {
            name: 'Obondegohan',
            url: 'https://tabelog.com/en/tokyo/A1305/A130501/13138302/',
            country: 'jp',
            address: '1-11-1 Nishiikebukuro Toshima Tokyo',
            link: 'http://obon-de-gohan.com/index.php',
            rating: 8,
            holiday: '不定休（ルミネ池袋店に準ずる）',
            isRelocated: false,
            location: {
                lat: 35.728854119159735,
                lng: 139.70947684349488,
            },
        };

        const expectedInput = {
            name: 'Obondegohan',
            url: 'https://tabelog.com/en/tokyo/A1305/A130501/13138302/',
            country: 'jp',
            address: '1-11-1 Nishiikebukuro Toshima Tokyo',
            link: 'http://obon-de-gohan.com/index.php',
            rating: 8,
            holiday: '不定休（ルミネ池袋店に準ずる）',
            isRelocated: false,
            location: {
                type: 'Point',
                coordinates: [
                    139.70947684349488,
                    35.728854119159735,
                ],
            },
        };

        mockedMongoDbConnection.getInstanceByCollection.mockReturnValue({
            updateOne: jest.fn(() => ({
                result: {
                    ok: 1,
                    n: 1,
                },
            })),
        });

        expect.assertions(2);
        const error = await RestaurantModel.put(id, input);

        expect(error).toBeUndefined();
        expect((mockedMongoDbConnection as any).getInstanceByCollection().updateOne)
            .toBeCalledWith({ _id: new ObjectId(id) }, expectedInput);
    });
});
