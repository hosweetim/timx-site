import * as Joi from 'joi';

export interface RestaurantInfo {
    name: string;
    url: string;
    country: string;
    address: string;
    link: string;
    holiday: string;
    rating: number;
    isRelocated: boolean;
    location: {
        lng: number;
        lat: number;
    };
}

export const RestaurantInfoSchema = Joi.object().keys({
    name: Joi.string().required(),
    url: Joi.string().uri().required(),
    country: Joi.string().min(2).max(2).required(),
    address: Joi.string().required(),
    link: Joi.string().uri().required().allow(''),
    holiday: Joi.string().required(),
    rating: Joi.number().required(),
    isRelocated: Joi.boolean().required(),
    location: Joi.object().keys({
        lng: Joi.number().required(),
        lat: Joi.number().required(),
    }),
});
