import * as Joi from 'joi';
import { ObjectId } from 'mongodb';

import { MongoDbConnection } from '../../config/connection';
import { Config } from '../../config/env';
import {
    RestaurantInfo,
    RestaurantInfoSchema,
} from './restaurant.schema';

const { MONGODB_COLLECTION_RESTAURANT } = Config;

export const RestaurantModel = {
    async get(): Promise<RestaurantInfo[]> {
        const restaurants = await MongoDbConnection.getInstanceByCollection(MONGODB_COLLECTION_RESTAURANT)
            .find()
            .toArray();

        return restaurants.map((x: any) => {
            const [ lng, lat ] = x.location.coordinates;
            const location = {
                lat,
                lng,
            };

            return { ...x, location };
        });
    },
    async create(restaurant: RestaurantInfo): Promise<[ Error | null, any ]> {
        const { error, value } = Joi.validate<RestaurantInfo>(restaurant, RestaurantInfoSchema);

        if (error) {
            throw(error);
        }

        const { location, ...others } = value;
        const { result, ops } = await MongoDbConnection.getInstanceByCollection(MONGODB_COLLECTION_RESTAURANT)
            .insertOne({
                ...others,
                location: {
                    type: 'Point',
                    coordinates: [
                        location.lng,
                        location.lat,
                    ],
                },
            });

        if (result.ok > 0
            && result.n === 1)
        {
            return [ null, ops ];
        }

        return [ new Error('insertion failed'), null ];
    },
    async put(id: string, restaurant: RestaurantInfo): Promise<Error | undefined> {
        const { error, value } = Joi.validate<RestaurantInfo>(restaurant, RestaurantInfoSchema);

        if (error) {
            throw(error);
        }

        const { location, ...others } = value;
        const { result } = await MongoDbConnection.getInstanceByCollection(MONGODB_COLLECTION_RESTAURANT)
            .updateOne({
                _id: new ObjectId(id),
            }, {
                ...others,
                location: {
                    type: 'Point',
                    coordinates: [
                        location.lng,
                        location.lat,
                    ],
                },
            });

        if (result.ok > 0
            && result.n === 1)
        {
            return;
        }

        return new Error('insertion failed');
    },
    async delete(id: string): Promise<Error | undefined> {
        const { result } = await MongoDbConnection.getInstanceByCollection(MONGODB_COLLECTION_RESTAURANT)
            .deleteOne({
                _id: new ObjectId(id),
            });

        if (result.n === 1) {
            return;
        }

        return new Error('insertion failed');
    },
};
