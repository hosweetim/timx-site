import { TabelogModel } from './tabelog';

import axios from 'axios';

test('convertToEnglishUrl', () => {
    const sample = [
        'https://tabelog.com/tw/tokyo/A1323/A132302/13194172',
        'https://tabelog.com/en/tokyo/A1323/A132302/13194172',
        'https://tabelog.com/kr/tokyo/A1323/A132302/13194172',
        'https://tabelog.com/tokyo/A1323/A132302/13194172',
    ];

    const expected = 'https://tabelog.com/en/tokyo/A1323/A132302/13194172';

    sample.forEach((s) => {
        expect(TabelogModel.convertToEnglishUrl(s)).toBe(expected);
    });
});

test('crawl', async () => {
    const sample = [
        {
            url: 'https://tabelog.com/en/tokyo/A1323/A132302/13194172/',
            expected: {
                name: 'Houei',
                url: 'https://tabelog.com/en/tokyo/A1323/A132302/13194172/',
                address: '5 Chome-38-14 Koishikawa Bunkyo-ku Tokyo-to',
                holiday: '水曜日・木曜日 祝日の場合定休日が異なる場合ございます。',
                link: '',
                isRelocated: false,
                location: {
                    lat: 35.71814752131738,
                    lng: 139.74327175200463,
                },
            },
        },
        {
            url: 'https://tabelog.com/en/tokyo/A1305/A130501/13003913/',
            expected: {
                name: 'Eraji',
                url: 'https://tabelog.com/en/tokyo/A1305/A130501/13003913/',
                address: '2-42-7 Minamiikebukuro Toshima Tokyo',
                holiday: '毎火曜日＆月曜の不定休あり',
                link: '',
                isRelocated: true,
                location: {
                    lat: 35.726179840141135,
                    lng: 139.71847427494325,
                },
            },
        },
    ];

    expect.assertions(sample.length);

    const res = await Promise.all(sample.map(async (s) => {
        const { data } = await axios.get(s.url);

        return {
            actual: TabelogModel.crawl(data),
            expected: s.expected,
        };
    }));

    res.forEach((r) => expect(r.actual).toStrictEqual(r.expected));
}, 10000);

