import { MongoClient, Collection, Db } from 'mongodb';
import * as assert from 'assert';

const {
    MONGODB_URL,
    MONGODB_NAME,
    MONGODB_USERNAME,
    MONGODB_PASSWORD,
    MONGODB_AUTH_SOURCE,
} = process.env;

if (!MONGODB_URL
    && !MONGODB_NAME
    && !MONGODB_PASSWORD
    && !MONGODB_USERNAME
    && !MONGODB_AUTH_SOURCE)
{
    throw new Error('MongoDB username, password, url and name not configured');
}

const DB_URL = `mongodb://${MONGODB_USERNAME}:${MONGODB_PASSWORD}@${MONGODB_URL}`;

export class MongoDbConnection {
    static db: Db;
    static async connect() {
        try {
            const client = await MongoClient.connect(DB_URL, {
                useNewUrlParser: true,
                authSource: MONGODB_AUTH_SOURCE,
            });

            this.db = client.db(MONGODB_NAME);

            return null;
        } catch (e) {
            return e;
        }
    }

    static getInstance(): Db {
        assert(this.db, 'MongoDB is not connected');

        return this.db;
    }

    static getInstanceByCollection(collection: string): Collection<any> {
        assert(this.db, 'MongoDB is not connected');

        return this.db.collection(collection);
    }
}
