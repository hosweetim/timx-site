import { Storage } from '@google-cloud/storage';
import { Bucket } from '@google-cloud/storage/build/src/bucket';

const {
    CLOUD_STORAGE_BUCKET_NAME,
    CLOUD_STORAGE_FOLDER_NAME,
    CLOUD_STORAGE_PROJECT_ID,
} = process.env;

if (!CLOUD_STORAGE_BUCKET_NAME
    && !CLOUD_STORAGE_FOLDER_NAME
    && !CLOUD_STORAGE_PROJECT_ID)
{
    throw new Error('Google cloud storage environment key not set');
}

const storage = new Storage({
    projectId: CLOUD_STORAGE_PROJECT_ID,
});

const bucket = storage.bucket(CLOUD_STORAGE_BUCKET_NAME || '');

export class CloudStorageConnection {
    static getInstance(): Bucket {
        return bucket;
    }
}
