import {
    RequestHandler,
    Request,
    Response,
    NextFunction,
} from 'express';

export const asyncHandler = (fn: RequestHandler) => async (req: Request, res: Response, next: NextFunction) => {
    try {
        await fn(req, res, next);
    } catch (e) {
        next(e);
    }
};
