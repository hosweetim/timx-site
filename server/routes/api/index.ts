import { Router } from 'express';

import blog from './blog';
import crawler from './crawler';
import markdown from './markdown';
import links from './links';
import restaurant from './restaurant';
import storage from './storage';

const router = Router();

router.use('/blog', blog);
router.use('/crawler', crawler);
router.use('/links', links);
router.use('/markdown', markdown);
router.use('/restaurant', restaurant);
router.use('/storage', storage);

export default router;
