import { Router } from 'express';

import { asyncHandler } from '../../util';
import { LinksController } from '../../controllers';

const router = Router();

router.get('/', asyncHandler(LinksController.get));
router.get('/:id', asyncHandler(LinksController.getById));
router.delete('/:id', asyncHandler(LinksController.deleteById));
router.post('/', asyncHandler(LinksController.post));

export default router;
