import { Router } from 'express';

import { asyncHandler } from '../../util';
import { MarkdownController } from '../../controllers';

const router = Router();

router.post('/', asyncHandler(MarkdownController.post));
router.get('/theme', asyncHandler(MarkdownController.theme.get));

export default router;
