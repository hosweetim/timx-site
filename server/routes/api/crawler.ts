import { Router } from 'express';

import { asyncHandler } from '../../util';
import { CrawlerController } from '../../controllers';

const router = Router();

router.get('/', asyncHandler(CrawlerController.get));
router.post('/', asyncHandler(CrawlerController.post));
router.get('/tabelog/:url', asyncHandler(CrawlerController.tabelog.get));

export default router;
