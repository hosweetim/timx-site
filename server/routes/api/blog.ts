import { Router } from 'express';

import * as mutler from 'multer';

import { asyncHandler } from '../../util';
import { BlogController } from '../../controllers';

const upload = mutler({
    storage: mutler.memoryStorage(),
});

const router = Router();

router.get('/', asyncHandler(BlogController.get));
router.post('/', upload.single('file'), asyncHandler(BlogController.post));
router.delete('/', asyncHandler(BlogController.delete));
router.get('/:id', asyncHandler(BlogController.getFromId));

export default router;
