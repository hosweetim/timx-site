import { Router } from 'express';

import { asyncHandler } from '../../util';
import { RestaurantController } from '../../controllers';

const router = Router();

router.get('/', asyncHandler(RestaurantController.get));
router.post('/', asyncHandler(RestaurantController.post));
router.put('/:id', asyncHandler(RestaurantController.put));
router.delete('/:id', asyncHandler(RestaurantController.delete));

export default router;
