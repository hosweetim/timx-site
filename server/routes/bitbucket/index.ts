import * as express from 'express';
import { Router } from 'express';
import * as path from 'path';

const router = Router();

router.use('/', express.static('static/bitbucket'));

router.get('*', (req, res) => {
    const bitbucketFile = path.resolve(
        __dirname,
        '..',
        '..',
        '..',
        'static',
        'bitbucket',
        'index.html');

    res.sendFile(bitbucketFile);
});

export default router;
