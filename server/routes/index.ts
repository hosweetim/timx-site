import { Router } from 'express';

import bitbucket from './bitbucket';
import api from './api';

const router = Router();

router.use('/api', api);
router.use('/bitbucket', bitbucket);

export default router;
