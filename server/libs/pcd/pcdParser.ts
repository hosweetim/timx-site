/**
 * This code is gotten from
 * https://gitlab.com/taketwo/three-pcd-loader
 * based on this question from stackoverflow
 * https://stackoverflow.com/questions/43932166/how-to-parse-binary-compressed-point-cloud
 */

import { PCDData } from './pcdData';

export class PCDParser {
    // static async load(url) {
    //     return new Promise((resolve, reject) => {
    //         const xhr = new XMLHttpRequest();
    //         xhr.open('GET', url, true);
    //         xhr.responseType = 'arraybuffer';

    //         xhr.onload = (e) => {
    //             if (this.status === 200) {
    //                 const loader = new PCDParser();
    //                 const pcd = loader.parse(this.response);
    //                 resolve(pcd);
    //                 return;
    //             }

    //             reject(e);
    //         };

    //         xhr.onerror = (e) => {
    //             reject(e);
    //         };

    //         xhr.send();
    //     });
    // }

    static parse(data: ArrayBuffer): PCDData {
        const pcdParser = new PCDParser();
        return pcdParser.parse(data);
    }

    private littleEndian: boolean;

    constructor() {
        this.littleEndian = true;
    }

    decompressLZF(inData: any, outLength: any) {
        const inLength = inData.length;
        const outData = new Uint8Array(outLength);
        let inPtr = 0;
        let outPtr = 0;
        let ctrl;
        let len;
        let ref;

        do {
            ctrl = inData[inPtr++];
            // tslint:disable-next-line:no-bitwise
            if (ctrl < (1 << 5)) {
                ctrl++;

                if (outPtr + ctrl > outLength) {
                    throw new Error('Output buffer is not large enough');
                }

                if (inPtr + ctrl > inLength) {
                    throw new Error('Invalid compressed data');
                }

                do {
                    outData[outPtr++] = inData[inPtr++];
                } while (--ctrl);
            } else {
                // tslint:disable-next-line:no-bitwise
                len = ctrl >> 5;
                // tslint:disable-next-line:no-bitwise
                ref = outPtr - ((ctrl & 0x1f) << 8) - 1;
                if (inPtr >= inLength) {
                    throw new Error('Invalid compressed data');
                }

                if (len === 7) {
                    len += inData[inPtr++];
                    if (inPtr >= inLength) {
                        throw new Error('Invalid compressed data');
                    }
                }

                ref -= inData[inPtr++];

                if (outPtr + len + 2 > outLength) {
                    throw new Error('Output buffer is not large enough');
                }

                if (ref < 0) {
                    throw new Error('Invalid compressed data');
                }

                if (ref >= outPtr) {
                    throw new Error('Invalid compressed data');
                }

                do {
                    outData[outPtr++] = outData[ref++];
                } while (--len + 2);
            }
        } while (inPtr < inLength);

        return outData;
    }
    parse(data: any): PCDData {
        const header = this.parseHeader(data);
        const offset = header.offset;

        const pcdData: PCDData = {
            x: [],
            y: [],
            z: [],
            color: [],
        };

        const color = false;
        let colorOffset;
        if (offset.rgb !== undefined || offset.rgba !== undefined) {
            colorOffset = offset.rgb === undefined ? offset.rgba : offset.rgb;
        }

        if (header.data === 'ascii') {
            const charArrayView = new Uint8Array(data);
            let dataString = '';
            for (let j = header.headerLen; j < data.byteLength; j++) {
                dataString += String.fromCharCode(charArrayView[j]);
            }

            const lines = dataString.trim().split('\n');
            let i3 = 0;

            for (let i = 0; i < lines.length; i++ , i3 += 3) {
                const line = lines[i].split(' ');

                pcdData.x.push(parseFloat(line[offset.x]));
                pcdData.y.push(parseFloat(line[offset.y]));
                pcdData.z.push(parseFloat(line[offset.z]));

                if (color !== false) {
                    let c: any;
                    if (offset.rgba !== undefined) {
                        c = new Uint32Array([ parseInt(line[offset.rgba], 10) ]);
                    } else if (offset.rgb !== undefined) {
                        c = new Float32Array([ parseFloat(line[offset.rgb]) ]);
                    }
                    const dataview = new Uint8Array(c.buffer, 0);
                    pcdData.color[i3 + 2] = dataview[0] / 255.0;
                    pcdData.color[i3 + 1] = dataview[1] / 255.0;
                    pcdData.color[i3 + 0] = dataview[2] / 255.0;
                }
            }
        } else if (header.data === 'binary') {
            let row = 0;
            const dataArrayView = new DataView(data, header.headerLen);

            for (let p = 0; p < header.points; row += header.rowSize, p++) {
                pcdData.x.push(dataArrayView.getFloat32(row + offset.x, this.littleEndian));
                pcdData.y.push(dataArrayView.getFloat32(row + offset.y, this.littleEndian));
                pcdData.z.push(dataArrayView.getFloat32(row + offset.z, this.littleEndian));
                pcdData.color[p * 3 + 2] = dataArrayView.getUint8(row + colorOffset + 0) / 255.0;
                pcdData.color[p * 3 + 1] = dataArrayView.getUint8(row + colorOffset + 1) / 255.0;
                pcdData.color[p * 3 + 0] = dataArrayView.getUint8(row + colorOffset + 2) / 255.0;
            }
        } else if (header.data === 'binary_compressed') {
            const sizes = new Uint32Array(data.slice(header.headerLen, header.headerLen + 8));
            const compressedSize = sizes[0];
            const decompressedSize = sizes[1];
            const decompressed = this.decompressLZF(
                new Uint8Array(
                    data,
                    header.headerLen + 8,
                    compressedSize),
                decompressedSize);

            const dataArrayView = new DataView(decompressed.buffer);
            for (let p = 0; p < header.points; p++) {
                pcdData.x.push(dataArrayView.getFloat32(offset.x + p * 4, this.littleEndian));
                pcdData.y.push(dataArrayView.getFloat32(offset.y + p * 4, this.littleEndian));
                pcdData.z.push(dataArrayView.getFloat32(offset.z + p * 4, this.littleEndian));

                pcdData.color[p * 3 + 2] = dataArrayView.getUint8(colorOffset + p * 4 + 0) / 255.0;
                pcdData.color[p * 3 + 1] = dataArrayView.getUint8(colorOffset + p * 4 + 1) / 255.0;
                pcdData.color[p * 3 + 0] = dataArrayView.getUint8(colorOffset + p * 4 + 2) / 255.0;
            }
        }

        return pcdData;
    }
    parseHeader(binaryData: any) {
        let headerText = '';
        const charArray = new Uint8Array(binaryData);
        let i = 0;
        const max = charArray.length;

        while (i < max && headerText.search(/[\r\n]DATA\s(\S*)\s/i) === -1) {
            headerText += String.fromCharCode(charArray[i++]);
        }
        const result1 = headerText.search(/[\r\n]DATA\s(\S*)\s/i);
        const result2: any = /[\r\n]DATA\s(\S*)\s/i.exec(headerText.substr(result1 - 1));

        const header: any = {};
        header.data = result2[1];
        header.headerLen = result2[0].length + result1;
        header.str = headerText.substr(0, header.headerLen);

        // Remove comments
        header.str = header.str.replace(/\#.*/gi, '');
        header.version = /VERSION (.*)/i.exec(header.str);
        if (header.version !== null) {
            header.version = parseFloat(header.version[1]);
        }
        header.fields = /FIELDS (.*)/i.exec(header.str);
        if (header.fields !== null) {
            header.fields = header.fields[1].split(' ');
        }
        header.size = /SIZE (.*)/i.exec(header.str);
        if (header.size !== null) {
            header.size = header.size[1].split(' ').map((x: any) => {
                return parseInt(x, 10);
            });
        }
        header.type = /TYPE (.*)/i.exec(header.str);
        if (header.type !== null) {
            header.type = header.type[1].split(' ');
        }
        header.count = /COUNT (.*)/i.exec(header.str);
        if (header.count !== null) {
            header.count = header.count[1].split(' ').map((x: any) => {
                return parseInt(x, 10);
            });
        }
        header.width = /WIDTH (.*)/i.exec(header.str);
        if (header.width !== null) {
            header.width = parseInt(header.width[1], 10);
        }
        header.height = /HEIGHT (.*)/i.exec(header.str);
        if (header.height !== null) {
            header.height = parseInt(header.height[1], 10);
        }
        header.viewpoint = /VIEWPOINT (.*)/i.exec(header.str);
        if (header.viewpoint !== null) {
            header.viewpoint = header.viewpoint[1];
        }
        header.points = /POINTS (.*)/i.exec(header.str);
        if (header.points !== null) {
            header.points = parseInt(header.points[1], 10);
        }
        if (header.points === null) {
            header.points = header.width * header.height;
        }
        if (header.count === null) {
            header.count = [];
            for (i = 0; i < header.fields; i++) {
                header.count.push(1);
            }
        }
        header.offset = {};
        let sizeSum = 0;
        for (let j = 0; j < header.fields.length; j++) {
            if (header.data === 'ascii') {
                header.offset[header.fields[j]] = j;
            } else if (header.data === 'binary') {
                header.offset[header.fields[j]] = sizeSum;
                sizeSum += header.size[j];
            } else if (header.data === 'binary_compressed') {
                header.offset[header.fields[j]] = sizeSum;
                sizeSum += header.size[j] * header.points;
            }
        }
        // For binary only
        header.rowSize = sizeSum;

        return header;
    }
}
