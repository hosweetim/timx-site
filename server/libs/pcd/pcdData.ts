export interface PCDData {
    x: number[];
    y: number[];
    z: number[];
    color: number[];
}
