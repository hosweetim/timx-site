import { PCDParser } from './pcdParser';
import { readFileSync } from 'fs';
import { join } from 'path';

test('PCDParser', async () => {
    const FILE_LISTS = [
        'test_ascii.pcd',
        'test_binary.pcd',
        'test_binaryCompressed.pcd',
    ];

    FILE_LISTS.forEach((fileName) => {
        const FILE_PATH = join(__dirname, 'sample', fileName);
        const content = readFileSync(FILE_PATH);
        const pcdData = PCDParser.parse(
            content.buffer.slice(content.byteOffset, content.byteOffset + content.byteLength),
        );

        expect(pcdData.x.length).toBe(5);
        expect(pcdData.y.length).toBe(5);
        expect(pcdData.z.length).toBe(5);

        for (let i = 0; i < pcdData.x.length; i++) {
            expect(pcdData.x[i]).toBe(i);
            expect(pcdData.y[i]).toBe(i + 2);
            expect(pcdData.z[i]).toBe(i + 3);
        }
    });
});
