module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    verbose: false,
    collectCoverage: false,
    coverageDirectory: 'reports/coverage',
    collectCoverageFrom: [
        '**/*.ts'
    ],
    coveragePathIgnorePatterns: [
        '/node_modules/',
        '/assets/',
        '/plugins/',
        '/types/',
        '/.nuxt/',
    ],
    reporters: [
        'default',
        [ 'jest-junit', { output: 'reports/unit_test/report.xml' }]
    ],
    testPathIgnorePatterns: [
        'dist/',
        'integration/'
    ],
    watchPathIgnorePatterns: [
        'dist/',
        'integration/'
    ],
    globals: {
        'ts-jest': {
            tsConfig: 'tsconfig.server.json',
            diagnostics: false
        }
    },
    testResultsProcessor: 'jest-sonar-reporter'
};
